import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import cors from 'cors';
import db from "./database.js";

dotenv.config();

const app: Express = express();
const port = 3100;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req: Request, res:Response)=>{
    var sql = "select * from feedback"
    db.all(sql, (err, rows) => {
        if (err) {
            console.log(err);
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
})

app.post('/', (req: Request, res: Response) => {
    var errors=[]
    if (!req.body.message){
        errors.push("No message added");
    }
    var data = {
        value: req.body.message,
    }
    console.log(req.body.message)
     var insert = 'INSERT INTO feedback (message) VALUES (?)'
     db.run(insert, data.value)

    res.send('Feedback added to database');
});

app.delete("/", (req: Request, res: Response) => {
    var del = 'DELETE FROM feedback';
    db.run(
        del);
    res.send('DB cleared');
})

app.listen(port, () =>  console.log(`⚡️[server]: Server is running at https://localhost:${port}`));
