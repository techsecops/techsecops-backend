# any future command that fails will exit the script
set -e

cd /home/ubuntu/techsecops-backend

# init SSH agent
eval $(ssh-agent -s)
#  echo "$GIT_KEY"
ssh-add ~/.ssh/id_rsa_gitlab

# git clone https://gitlab.com/techsecops/techsecops-backend.git
git pull

echo which node
echo which npm
echo which pm2

PATH="/home/ubuntu/.nvm/versions/node/v16.3.0/bin:$PATH";

echo "RUN INSTALL DEPS"

# /home/ubuntu/.nvm/versions/node/v14.5.0/bin/npm i
npm i

npm run build

echo "RESTART PM2"

# /home/ubuntu/.nvm/versions/node/v14.5.0/bin/pm2 restart all
pm2 restart all
