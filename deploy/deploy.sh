#!/bin/bash

# any future command that fails will exit the script
set -e

# add private key to .pem file
echo "$PRIVATE_KEY"
# echo eval `ssh-agent -s`
# echo touch ~/.ssh/config
# echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
# echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add -
# echo "$GIT_KEY" | tr -d '\r' | ssh-add -

# echo  -e "$PRIVATE_KEY" > prod.pem
# chmod 600 prod.pem
echo "$DEPLOY_SERVER"

# disable the host key checking.
chmod +x ./deploy/disableHostKeyChecking.sh
./deploy/disableHostKeyChecking.sh

ssh -i "$PRIVATE_KEY" ubuntu@54.169.178.111 'bash -s' < ./deploy/updateAndRestart.sh
