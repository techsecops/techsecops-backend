import sqlite3 from 'sqlite3';

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        db.run(`CREATE TABLE feedback (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            message text,
            timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            )`,

        (err) => {
            if (err) {
                console.log('Table already exists')
            }
        });
    }
});


export = db
